import requests

url = "http://api.github.com/user"

headers = {
    'Authorization': 'Bearer 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',
    'Accept': 'application/json'
}



response = requests.get(url)


if response.status_code == 200:
    usuario = response.json()

    print("Información del usuario de GitHub: ")
    print(f"Nombre: {usuario['name']} " )
    print(f"Nombre de Usuario: {['login']}")
    print(f"ID: {['id']}")
    print(f"Seguidores: {['followers']}")
else:
    print(f"Error al realizar la solicitud, código: {response.status_code}")
