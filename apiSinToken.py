import requests


url = "https://jsonplaceholder.typicode.com/todos"

# realizamos la solicitud GET
response = requests.get(url)

# Verificar que la respuesta es exitosa
if response.status_code == 200:
    todos = response.json()

    for t in todos[:5]:
        print(f"ID: {t['id']}, Título: {t['title']}, Completada: {['completed']}")
    
else:
    print(f"Error al realizar la solicitud, código: {response.status_code}")




def metodoPost():
    url = "https://jsonplaceholder.typicode.com/todos"

    datosPost = {
        "title" : "Post Python Miguel",
        "body" : "Metodo post envía datos a la api, respetando la estructura de datos de esta",
        "userId" : "1"
    }

    response = requests.post(url, json=datosPost)

    if response.status_code == 201:
        ultimoPost = response.json()
        print(f"El titulo del nuevo post es: {ultimoPost['title']} , y dice: {ultimoPost['body']}")
    else:
        print(f"Error al realizar la solicitud, código: {response.status_code}")

metodoPost()